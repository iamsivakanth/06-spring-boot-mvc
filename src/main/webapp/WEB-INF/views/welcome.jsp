<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>Download Excel</title>
</head>
<body>
	<h2 style="color: Green">Hello Spring Boot MVC</h2>
	<h1>Download Excel File</h1>
	<a href="<c:url value='/downloadExcel' />">Click here to download Excel</a>
</body>
</html>