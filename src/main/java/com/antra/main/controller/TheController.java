package com.antra.main.controller;

import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.antra.main.entity.Person;
import com.antra.main.entity.PersonRepo;

@Controller
public class TheController {
	
	@Autowired
	PersonRepo repo;

	@GetMapping(value = "/hello")
	public String getWelcome() {
		return "welcome";
	}

	@GetMapping("/downloadExcel")
	public void downloadExcel(HttpServletResponse response) {
		List<Person> persons = repo.findAll();
		System.out.println("Persons Data : " + persons);
		
		try (Workbook workbook = new XSSFWorkbook()) {
			// Create a sheet and populate it with data
			Sheet sheet = workbook.createSheet("Sample Sheet");

			Row headerRow = sheet.createRow(0);
			headerRow.createCell(0).setCellValue("ID");
			headerRow.createCell(1).setCellValue("Name");
			headerRow.createCell(2).setCellValue("Age");

			for (int i = 0; i < persons.size(); i++) {
				Row dataRow = sheet.createRow(i + 1);
				dataRow.createCell(0).setCellValue(persons.get(i).getId());
				dataRow.createCell(1).setCellValue(persons.get(i).getName());
				dataRow.createCell(2).setCellValue(persons.get(i).getAge());
			}

			// Set the response headers
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", "attachment; filename=example.xlsx");

			// Write the workbook to the response output stream
			try (OutputStream out = response.getOutputStream()) {
				workbook.write(out);
			}
		} catch (Exception e) {
			throw new RuntimeException("Error while generating Excel file", e);
		}
	}
}